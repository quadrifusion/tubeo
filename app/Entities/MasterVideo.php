<?php
namespace App\Entities;


/**
 * Classe representant une video a afficher sur la page d'accueil
 */

class MasterVideo
{
    protected $title;

    protected $duration;

    protected $id;

    public function __construct($title, $duration, $id)
    {            
        $this->title = $title;
        $this->duration = $duration;
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}


?>