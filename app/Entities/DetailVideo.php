<?php
namespace App\Entities;

/**
 * Classe representant une video sur la page de detail
 */

class DetailVideo
{
    protected $id;

    protected $rawTitle;

    protected $path;

    protected $title;

    protected $screenshots;

    protected $mimeType;

    public function __construct($rawTitle, $title, $id, $screenshots, $path, $mimeType)
    {
        $this->title = $title;
        $this->rawTitle = $rawTitle;
        $this->id = $id;
        $this->screenshots = $screenshots;
        $this->path = $path;
        $this->mimeType = $mimeType;
    }

    public function getRawTitle()
    {
        return $this->rawTitle;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getScreenshots()
    {
        return $this->screenshots;
    }

    public function setRawTitle($rawTitle)
    {
        $this->rawTitle = $rawTitle;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setScreenshots($screenshots)
    {
        $this->screenshots = $screenshots;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

}
