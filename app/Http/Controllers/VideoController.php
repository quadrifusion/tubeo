<?php

namespace App\Http\Controllers;

use App\Entities\DetailVideo;
use App\Entities\MasterVideo;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use FFMpeg;

class VideoController extends Controller {

    public function index() {
        $cheminProjet = base_path(). DIRECTORY_SEPARATOR;
        $string = "";
        $videoIndex = 1;

        // on instancie ffmpeg avec ffprobe
        $ffmpeg = FFMpeg\FFMpeg::create();
        $ffprobe = FFMpeg\FFProbe::create();

        // on recupere les fichiers du dossier video
        $fichiers = VideoController::recupFichiersHierarchie($cheminProjet."public" . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR, "videos", []);

        $videos = [];

        // Pour chaque fichier
        foreach ($fichiers as $fichier) {

            // Si le fichier est une video
            $fileMetaDataArray = pathinfo($fichier['chemin']. DIRECTORY_SEPARATOR . $fichier['nom']);
            if ($fileMetaDataArray['extension']) {
                if (in_array($fileMetaDataArray['extension'], ["mp4", "mkv", "avi", "mov", "flv", "wmv", "webm"])) {
                    // On recupere la duree de la video
                    $video = $ffprobe->format($cheminProjet."public" . DIRECTORY_SEPARATOR .$fichier['chemin']. DIRECTORY_SEPARATOR .$fichier['nom']);
                    $duration = round($video->get('duration') / 60) . ":" . $video->get('duration') % 60;

                    // On recupere le nom du fichier et on le transforme
                    $string = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fichier['nom']);
                    $nomVideo = pathinfo($fichier['nom'], PATHINFO_FILENAME);

                    // On recupere la miniature
                    if (!file_exists($cheminProjet."public" . DIRECTORY_SEPARATOR . "miniatures" . DIRECTORY_SEPARATOR . $nomVideo . "-miniature.jpg")) {
                        $this->genererMiniature($cheminProjet."public" . DIRECTORY_SEPARATOR . $fichier['chemin']. DIRECTORY_SEPARATOR .$fichier['nom'], $nomVideo, $video->get('duration'));
                    }

                    // On ajoute la video dans un tableau
                    array_push($videos, new MasterVideo($string, $duration, $videoIndex));

                    $videoIndex++;
                }
            }
        }

        // On affiche la page d'accueil avec les videos
        return view('masterVideo', ['videos' => $videos]);
    }

    public function detail($videoIndex) {
        // Verifie si l'index est un entier
        if (!((string) (int) $videoIndex == $videoIndex)) {
            return view('notfound_page');
        }
        $cheminProjet = base_path(). DIRECTORY_SEPARATOR;
        $string = "";

        // Initialise FFmpeg avec FFprobe
        $ffmpeg = FFMpeg\FFMpeg::create();
        $ffprobe = FFMpeg\FFProbe::create();

        $videoIndexTmp = 1;

        // Recuperation des fichiers du dossier video
        $fichiers = VideoController::recupFichiersHierarchie($cheminProjet."public" . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR, "videos", []);
        
        // Pour chaque fichier
        foreach ($fichiers as $fichier) {

            // Si le fichier est une video
            $fileMetaDataArray = pathinfo($fichier['chemin']. DIRECTORY_SEPARATOR . $fichier['nom']);
            if ($fileMetaDataArray['extension']) {
                if (in_array($fileMetaDataArray['extension'], ["mp4", "mkv", "avi", "mov", "flv", "wmv",  "webm"])) {

                    // Si la video est la video souhaitee
                    if ($videoIndex == $videoIndexTmp) {

                        // On recupere la duree de la video
                        $video = $ffprobe->format($cheminProjet."public" . DIRECTORY_SEPARATOR .$fichier['chemin']. DIRECTORY_SEPARATOR .$fichier['nom']);
                        $duration = round($video->get('duration') / 60) . ":" . $video->get('duration') % 60;
                        $nomVideo = pathinfo($fichier['nom'], PATHINFO_FILENAME);

                        // Genere les captures si besoin
                        $this->genererCaptures($cheminProjet."public" . DIRECTORY_SEPARATOR . $fichier['chemin']. DIRECTORY_SEPARATOR .$fichier['nom'], $nomVideo, $video->get('duration'), 10); // Gerer le nombre de capture avec un parametre

                        // On met le chemin des captures dans un tableau pour le front-end
                        $screenshots = [];
                        $screenshotsFiles = scandir($cheminProjet."public" . DIRECTORY_SEPARATOR . "captures" . DIRECTORY_SEPARATOR. $nomVideo);
                        foreach ($screenshotsFiles as $value) {
                            if (strpos($value, 'capture') !== false) {
                                array_push($screenshots, $value);
                            }
                        }

                        // On devine le type de fichier en fonction de l'extension
                        $mimeTypeGuesser = MimeTypeGuesser::getInstance();
                        $mimeType = $mimeTypeGuesser->guess($cheminProjet."public" . DIRECTORY_SEPARATOR . $fichier['chemin']. DIRECTORY_SEPARATOR .$fichier['nom']);

                        // On instancie une video 
                        $videoEntity = new DetailVideo($fichier['nom'], $nomVideo, $videoIndex, $screenshots, $fichier['chemin'], $mimeType);

                        // On affiche la page de detail
                        return view('detailVideo', ['video' => $videoEntity]);
                    } else {
                        // Si la video n'est pas la bonne
                        $videoIndexTmp++;
                    }
                }
            }
        }
        return view('notfound_page');
    }

    public function genererCaptures($cheminVideo, $nomVideo, $duration, $nbCaptures) {
        $cheminProjet = base_path(). DIRECTORY_SEPARATOR;

        // Instanciation de FFmpeg
        $ffmpeg = FFMpeg\FFMpeg::create();

        // Ouverture de la video dans FFmpeg
        $video = $ffmpeg->open($cheminVideo);
        $duree = $duration;

        // moment entre chaque capture
        $secondesCapture = $duree / $nbCaptures;

        // Seconde a laquelle la capture
        $keyframe = 0;

        // Si le dossier 'captures' n'existe pas
        if (!file_exists($cheminProjet."public" . DIRECTORY_SEPARATOR . "captures" . DIRECTORY_SEPARATOR)) {
            // On creer le dossier 'captures'
            mkdir($cheminProjet."public" . DIRECTORY_SEPARATOR . "captures" . DIRECTORY_SEPARATOR, 0777, true);
        }

        // Si le dossier des captures de la videos n'existe pas
        if (!file_exists($cheminProjet."public" . DIRECTORY_SEPARATOR . "captures" . DIRECTORY_SEPARATOR . $nomVideo)) {
            // Creer le dossier des captures de la video
            mkdir($cheminProjet."public" . DIRECTORY_SEPARATOR . "captures" . DIRECTORY_SEPARATOR . $nomVideo, 0777, true);

            // Pour le nombre de captures
            for ($i = 1; $i <= $nbCaptures; $i++) {
                // Generation de la capture
                $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(floor($keyframe)))->save($cheminProjet."public" . DIRECTORY_SEPARATOR . "captures" . DIRECTORY_SEPARATOR .$nomVideo. DIRECTORY_SEPARATOR . "capture-$i.jpg");
                
                // On ajoute le nombre de seconde entre chaque capture
                $keyframe += $secondesCapture;
            }
        }
    }

    public function genererMiniature($cheminVideo, $nomVideo, $duration) {
        $cheminProjet = base_path(). DIRECTORY_SEPARATOR;
        // Instanciation de FFmpeg
        $ffmpeg = FFMpeg\FFMpeg::create();

        // Ouverture de la video dans FFmpeg
        $video = $ffmpeg->open($cheminVideo);
        $duree = $duration;

        // Moment de la capture (au milieu de la video)
        $secondesCapture = $duree / 2;
        // Si le dossier 'miniatures' n'existe pas
        if (!file_exists($cheminProjet."public" . DIRECTORY_SEPARATOR . "miniatures" . DIRECTORY_SEPARATOR)) {
            // On creer le dossier 'miniatures'
            mkdir($cheminProjet."public" . DIRECTORY_SEPARATOR . "miniatures" . DIRECTORY_SEPARATOR, 0777, true);
        }

        // Generation de la miniature au milieu de la video
        $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(floor($secondesCapture)))->save($cheminProjet."public" . DIRECTORY_SEPARATOR . "miniatures" . DIRECTORY_SEPARATOR . $nomVideo . "-miniature.jpg");

        // Redimensionne l'image generee par FFMpeg pour avoir une miniature
        VideoController::smart_resize_image($cheminProjet."public" . DIRECTORY_SEPARATOR . "miniatures" . DIRECTORY_SEPARATOR . $nomVideo . "-miniature.jpg", null, 160, 120, true, $cheminProjet."public" . DIRECTORY_SEPARATOR . "miniatures" . DIRECTORY_SEPARATOR . $nomVideo . "-miniature.jpg");
    }

    public static function recupFichiersHierarchie($cheminDossier, $nomDossierParent, $fichiersArray) {
        // Ouvre le dossier
        $iter = new \DirectoryIterator($cheminDossier);

        foreach ($iter as $item) {
            // Pour chaque fichier/dossier
            if ($item != '.' && $item != '..') {
                // Si l'element est un dossier
                if ($item->isDir()) {
                    // On appelle la fonction recursivement dans le sous-dossier
                    $tmp = VideoController::recupFichiersHierarchie($cheminDossier.$item.DIRECTORY_SEPARATOR, $nomDossierParent . "/" . $item, $fichiersArray);
                    // On ajoute les fichiers du sous-dossier
                    foreach ($tmp as $value) {
                        if (!in_array($value, $fichiersArray)) {
                            $fichiersArray[] = $value;
                        }
                    }
                } else {
                    // Renomme les fichiers pour enlever les caracteres ambigues
                    //rename($item->getPath().DIRECTORY_SEPARATOR.$item->getFileName(), preg_replace('/[^(\x20-\x7F)]*/', '', $item->getPath().DIRECTORY_SEPARATOR.$item->getFileName()));
                    
                    // Ajoute les fichiers avec leur chemin et leur nom
                    $fichiersArray[] = ['chemin' => $nomDossierParent, 'nom' => $item->getFilename()];
                }
            }
        }

        return $fichiersArray;
    }

    /**
     * Fonction pour redimensionner un image
     * 
     * https://github.com/Nimrod007/PHP_image_resize
     *
     * @param  $file - file name to resize
     * @param  $string - The image data, as a string
     * @param  $width - new image width
     * @param  $height - new image height
     * @param  $proportional - keep image proportional, default is no
     * @param  $output - name of the new file (include path if needed)
     * @param  $delete_original - if true the original image will be deleted
     * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
     * @param  $quality - enter 1-100 (100 is best quality) default is 100
     * @param  $grayscale - if true, image will be grayscale (default is false)
     * @return boolean|resource
     */
    public static function smart_resize_image($file,
        $string = null,
        $width = 0,
        $height = 0,
        $proportional = false,
        $output = 'file',
        $delete_original = true,
        $use_linux_commands = false,
        $quality = 100,
        $grayscale = false
    ) {

        if ($height <= 0 && $width <= 0) {
            return false;
        }

        if ($file === null && $string === null) {
            return false;
        }

        # Setting defaults and meta
        $info = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
        $image = '';
        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;
        $cropHeight = $cropWidth = 0;

        # Calculating proportionality
        if ($proportional) {
            if ($width == 0) {
                $factor = $height / $height_old;
            } elseif ($height == 0) {
                $factor = $width / $width_old;
            } else {
                $factor = min($width / $width_old, $height / $height_old);
            }

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        } else {
            $final_width = ($width <= 0) ? $width_old : $width;
            $final_height = ($height <= 0) ? $height_old : $height;
            $widthX = $width_old / $width;
            $heightX = $height_old / $height;

            $x = min($widthX, $heightX);
            $cropWidth = ($width_old - $width * $x) / 2;
            $cropHeight = ($height_old - $height * $x) / 2;
        }

        # Loading image to memory according to type
        switch ($info[2]) {
        case IMAGETYPE_JPEG:$file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);
            break;
        case IMAGETYPE_GIF:$file !== null ? $image = imagecreatefromgif($file) : $image = imagecreatefromstring($string);
            break;
        case IMAGETYPE_PNG:$file !== null ? $image = imagecreatefrompng($file) : $image = imagecreatefromstring($string);
            break;
        default:return false;
        }

        # Making the image grayscale, if needed
        if ($grayscale) {
            imagefilter($image, IMG_FILTER_GRAYSCALE);
        }

        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image);
            $palletsize = imagecolorstotal($image);

            if ($transparency >= 0 && $transparency < $palletsize) {
                $transparent_color = imagecolorsforindex($image, $transparency);
                $transparency = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            } elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);

        # Taking care of original, if needed
        if ($delete_original) {
            if ($use_linux_commands) {
                exec('rm ' . $file);
            } else {
                @unlink($file);
            }

        }

        # Preparing a method of providing result
        switch (strtolower($output)) {
        case 'browser':
            $mime = image_type_to_mime_type($info[2]);
            header("Content-type: $mime");
            $output = null;
            break;
        case 'file':
            $output = $file;
            break;
        case 'return':
            return $image_resized;
            break;
        default:
            break;
        }

        # Writing image according to type to the output destination and image quality
        switch ($info[2]) {
        case IMAGETYPE_GIF:imagegif($image_resized, $output);
            break;
        case IMAGETYPE_JPEG:imagejpeg($image_resized, $output, $quality);
            break;
        case IMAGETYPE_PNG:
            $quality = 9 - (int) ((0.9 * $quality) / 10.0);
            imagepng($image_resized, $output, $quality);
            break;
        default:return false;
        }

        return true;
    }

}
