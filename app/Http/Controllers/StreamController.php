<?php

namespace App\Http\Controllers;

use App\Entities\DetailVideo;
use App\Entities\MasterVideo;
use App\Entities\VideoStream;
use Symfony\Component\HttpFoundation\StreamedResponse;
use FFMpeg;

/**
 * Classe permettant le streaming 
 */
class StreamController extends Controller {

    /**
     * Streaming de la video a l'index voulue
     */
    public function stream($videoIndex) {
        // Verifie si l'index est un entier
        if (!((string) (int) $videoIndex == $videoIndex)) {
            return response("La vidéo n'existe pas.", 404);
        }
        $cheminProjet = base_path(). DIRECTORY_SEPARATOR; 
        $videoIndexTmp = 1;

        // Recuperation des fichiers du dossier video
        $fichiers = VideoController::recupFichiersHierarchie($cheminProjet."public" . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR, "videos", []);
        
        // Pour chaque fichier
        foreach ($fichiers as $fichier) {

            // Verifie si le fichier est une video
            $fileMetaDataArray = pathinfo($fichier['chemin']. DIRECTORY_SEPARATOR . $fichier['nom']);
            if ($fileMetaDataArray['extension']) {
                if (in_array($fileMetaDataArray['extension'], ["mp4", "mkv", "avi", "mov", "flv", "wmv", "webm"])) {

                    // Si la video est la video souhaitee
                    if ($videoIndex == $videoIndexTmp) {

                        // Instanciation de la classe VideoStream avec le fichier video
                        $stream = new VideoStream($fichier['chemin']. DIRECTORY_SEPARATOR . $fichier["nom"]);

                        // Demarre le streaming
                        $response = new StreamedResponse();
                        $response->setCallback(function () use ($stream) {
                            $stream->start();
                        });
    
                        return $response;
                    } else {
        
                        // Si ce n'est pas la bonne video, alors on passe a la suivante
                        $videoIndexTmp++;
                    }
                }
            }
        }

        return response("La vidéo n'existe pas.", 404);
    }
}
