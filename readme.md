# Tubeo Documentation 📺

Pour PHP 7.2.*
Micro-Framework Lumen de Laravel
Fonctionne avec PHP, Composer et FFmpeg

## Installation (Ubuntu/Windows)

Pour installer Composer :
 [getcomposer.org](https://getcomposer.org/download/)

Pour installer PHP :
* Windows : [php.net](https://secure.php.net/manual/fr/install.windows.php)
* Linux : [php.net](https://secure.php.net/manual/fr/install.unix.php)

Pour installer les dépendances PHP sur Ubuntu (déjà inclues dans Windows) : 

```$ apt install php7.2-gd2 php7.2-xml php7.2-zip``` 

Pour installer FFmpeg : 
* Sur Ubuntu :

```$ apt install ffmpeg```
* Sur Windows :
    Il faut télécharger FFmpeg et FFprobe en version 4.0 sur le site [ffbinaries.com](http://ffbinaries.com/downloads)
    Ensuite il faut mettre les deux fichiers executables dans un dossier et ajouter le chemin de ce dossier dans la variable d'environnment "PATH" de Windows

Il faut ensuite activer les extensions suivantes dans votre fichier `php.ini` :
* extension=openssl
* extension=mbstring
* extension=gd2
* extension=fileinfo

### Installation sans git
Si vous avez reçu le projet tubéo fournis avec un dossier `vendor` à la racine et un fichier `.env`, vous n'avez normalement pas besoin d'utiliser composer et de paramétrer le fichier d'environnement.

### Installation avec git
En premier lieu, il faut télécharger le répertoire git du projet :

```$ git clone https://gitlab.com/quadrifusion/tubeo.git```

Apres avoir cloné le répertoire git, placez-vous à la racine du projet `tubeo` et executez la commande suivante avec votre terminal :

```$ composer install```

Ensuite créez votre propre fichier d'environnement Lumen et nommé le `.env` (le fichier `.env.example` est un exemple de fichier d'environnement).

Le contenu de notre fichier d'environnement est le suivant :
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=file
QUEUE_DRIVER=sync
```

## Pour lancer un serveur de développement

```$ php -S localhost:8000 -t public```

## Dossier de vidéos 
Vous pouvez importer vos propre vidéos en les ajoutant à la racine du dossier `public/videos`.

## Pour utiliser l'application
La page d'accueil répertorie toutes les vidéos trouvées dans `public/videos`, elles s'afficheront sur la page d'accueil avec leur titre et leur durée. Si vous cliquez sur une vidéo, vous serez diriger vers une page détaillée de la vidéo. Cette page détaillée contient la vidéo disponible en streaming ainsi que 10 captures. Si vous cliquez sur une capture, elle s'agrandie sur votre écran.

## Librairies utilisées
* [Lumen](http://lumen.laravel.com/docs)
* [PHP-FFMpg](https://github.com/PHP-FFMpeg/PHP-FFMpeg)
* [zoom-vanilla-js](https://github.com/spinningarrow/zoom-vanilla.js/)