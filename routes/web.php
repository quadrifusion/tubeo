<?php
use App\Entities\MasterVideo;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\StreamController;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Route de la page d'accueil
$router->get('/', 'VideoController@index');

// Route de la page de détail d'une vidéo
$router->get('/{videoIndex}', [
    'as' => 'video', 'uses' => 'VideoController@detail'
]);

// Route permettant de servir une video en streaming
$router->get('/stream/{videoIndex}', [
    'as' => 'stream', 'uses' => 'StreamController@stream'
]);