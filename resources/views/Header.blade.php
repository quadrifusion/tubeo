<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <title>Tubeo</title>
    <link rel="stylesheet" href='css/header.css'>
    <link rel="stylesheet" href='css/style.css'>
    <link rel="stylesheet" href='css/videoplayer.css'>
    <script src="js/videoplayer.js"></script>
    <link rel="stylesheet" href='css/image-zoom.css'>
</head>

<body>
<div class="header_gradient">
</div>
<div class="header">
<div class="logo-container">
  <a href="/" class="logo">
    <img class="logo_img" src="/images/tubeo_logo.svg" alt="">
  </a>
</div>
</div>
<div class="fixed">
</div>
