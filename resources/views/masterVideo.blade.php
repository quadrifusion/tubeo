@component('Header')
@endcomponent('Header')

    <div class="container">
        <div class="wrapContainer">
            @foreach ($videos as $video)     
                @component ('thumbnail', [
                    'title' => $video->getTitle(),
                    'duration' => $video->getDuration(),
                    'id' => $video->getId()
                ])
                @endcomponent
            @endforeach
        </div>
    </div>
    
</body>
</html>

