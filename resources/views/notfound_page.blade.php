    @component('/Header')
    @endcomponent('/Header')
<link rel="stylesheet" href='css/notfound_page.css'>
<link rel="stylesheet" href='css/header.css'>
<div class="container_404">
    <div class="center_container_404">
    <div>
        <img class="img_error_404" src="images/tubeo_favicon.png">
    </div>
    <div class="text_404">
        <div class="column_404 big_size_404">Oups !</div>
        <div class="column_404">Page introuvable...</div>
    </div>
    </div>
</div>
