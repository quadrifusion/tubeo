@component ('Header')
@endcomponent
<div class="fullPageContainer">

@component ('backButton')
@endcomponent

    <div class="videoColumnContainer">
            <video id="videoplayer" class="video-js vjs-default-skin vjs-big-play-centered"
            controls preload="auto" width="700">

                <source src="/stream/{{$video->getId()}}" type="{{$video->getMimeType()}}"  />
            </video>
        <div class="belowVideoContainer">
            <div class="video_title">
                {{ $video->getTitle() }}
            </div>
            <a href="{{$video->getPath()}}/{{$video->getRawTitle()}}" download>
                <div class="download_button">
                    <div class="download_text">
                          Télécharger
                    </div>
                    <div class="download_icon">
                        <img src="images/download_button.png">
                    </div>
                </div>
            </a>
        </div>

    <div class="imagesColumnContainer">
        @foreach ($video->getScreenshots() as $screenshot)
            <img src="/captures/{{$video->getTitle()}}/{{$screenshot}}" class="imageFromVideo" alt="thumbnail" data-action="zoom">
        @endforeach
    </div>
</div>
</body>
<script src="js/image-zoom.js"></script>
</html>