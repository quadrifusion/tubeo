<div class="videoLink">
    <a href="{{ route('video', ['videoIndex'=>$id]) }}">
        <div class="videoContainer">
            <div class="thumbnailContainer">
                <img src="miniatures/{{ $title }}-miniature.jpg"
                        alt="thumbnail" class="thumbnail">
                <p class="duration">{{ $duration }}</p>
            </div>
            <div class="videoTitle">
                {{ $title }}
            </div>
        </div>
    </a>
</div>